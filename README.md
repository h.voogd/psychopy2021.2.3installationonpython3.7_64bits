This project installs Psychopy version 2021.2.3 and the rusocsci, seaborn, statsmodels, spyder, virtualenv and virtualenvwrapper-win packages.

**Usage:**

download '_requirementsPsychopy2021.2.3.64bit.txt_' and use:

mkvirtualenv -p37 Psychopy2021.2.3

pip install -r requirementsPsychopy2021.2.3.64bit.txt

**or (if you are using Windows):**

download both '_requirementsPsychopy2021.2.3.64bit.txt_' and '_InstallPsychopy2021.2.3.bat_'. Open a command window, go to the folder where both files reside and type:

InstallPsychopy2021.2.3
